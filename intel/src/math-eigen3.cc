#include <boost/array.hpp>
#include <boost/format.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/test/unit_test.hpp>
#include <iostream>

#pragma GCC diagnostic ignored "-Wdeprecated-copy"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#define EIGEN_ENABLE_AVX512
#define EIGEN_NO_DEBUG 1
#define EIGEN_NO_STATIC_ASSERT 1
#define EIGEN_MAX_ALIGN_BYTES 64
#define EIGEN_STACK_ALLOCATION_LIMIT (8 * 336 * 336)
#include <Eigen/Dense>

// Number of cycles to measure over.
extern int CYCLES;

// A timestamp.
static inline double timestamp() {
  static double ten_to_the_nine = 1000000000.0;
  static struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  double retval = t.tv_nsec;
  retval /= ten_to_the_nine;
  retval += t.tv_sec;
  return retval;
}

template <typename T, int R, int C>
void fill(Eigen::Matrix<T, R, C> &x, boost::random::uniform_real_distribution<T> &dist, boost::mt19937 &gen) {
  for (int r = 0; r < R; r++) {
    for (int c = 0; c < C; c++) {
      x(r, c) = dist(gen);
    }
  }
}

BOOST_AUTO_TEST_CASE(eigen3) {
  std::cout << "-------------------" << std::endl
            << boost::format("Eigen 3.%d.%d, GCC %s, M/N/cycles: %d, %d, %d") % EIGEN_MAJOR_VERSION % EIGEN_MINOR_VERSION % __VERSION__ % MAPS_M %
                   MAPS_N % CYCLES
            << std::endl;
  std::cout << "Processor         : " << std::flush;
  system("lscpu | grep 'Model name' | sed -e 's/Model name:[ ]*//'");

  // Make random number generator.
  boost::mt19937 gen(1382958545);  // The 16th Bell number. https://oeis.org/A000110
  boost::random::uniform_real_distribution<double> dist(-1.0, 1.0);

  // Get data ready.
  Eigen::Matrix<double, MAPS_M, MAPS_N> Xm;
  Eigen::Matrix<double, MAPS_N, 1> Yv;
  Eigen::Matrix<double, MAPS_M, 1> Zv;
  fill(Xm, dist, gen);
  fill(Yv, dist, gen);

  // Get ready record statistics about performance.
  boost::accumulators::accumulator_set<double,                                                          //
                                       boost::accumulators::stats<boost::accumulators::tag::mean,       //
                                                                  boost::accumulators::tag::max,        //
                                                                  boost::accumulators::tag::min,        //
                                                                  boost::accumulators::tag::variance>>  //
      loopStats = {};

  double u, v;
  for (int i = 0; i < CYCLES; i++) {
    // Set up for this iteration.
    fill(Xm, dist, gen);
    fill(Yv, dist, gen);

    u = timestamp();
    Zv = Xm * Yv;
    v = timestamp();
    loopStats(v - u);
  }

  std::cout << boost::format("avg (min/max/var) : %g (%g, %g, %g) seconds") % boost::accumulators::mean(loopStats) %
                   boost::accumulators::min(loopStats) % boost::accumulators::max(loopStats) % boost::accumulators::variance(loopStats)
            << std::endl;
}