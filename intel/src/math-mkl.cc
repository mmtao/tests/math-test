#include <mkl.h>

#include <array>
#include <boost/format.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/test/unit_test.hpp>
#include <iostream>

#pragma GCC diagnostic ignored "-Wdeprecated-copy"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

// Number of cycles to measure over.
extern int CYCLES;

// A timestamp.
static inline double timestamp() {
  static double ten_to_the_nine = 1000000000.0;
  static struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  double retval = t.tv_nsec;
  retval /= ten_to_the_nine;
  retval += t.tv_sec;
  return retval;
}

template <typename T, long unsigned int N>
void fill(std::array<T, N> &arr, boost::random::uniform_real_distribution<T> &dist, boost::mt19937 &gen) {
  for (unsigned int i = 0; i < N; i++) {
    arr[i] = dist(gen);
  }
}

BOOST_AUTO_TEST_CASE(mkl) {
  std::cout << "-------------------" << std::endl
            << boost::format("Intel MKL %d, GCC %s, M/N/cycles: %d, %d, %d") % INTEL_MKL_VERSION % __VERSION__ % MAPS_M % MAPS_N % CYCLES
            << std::endl;
  std::cout << "Processor         : " << std::flush;
  system("lscpu | grep 'Model name' | sed -e 's/Model name:[ ]*//'");

  // Make random number generator.
  boost::mt19937 gen(1382958545);  // The 16th Bell number. https://oeis.org/A000110
  boost::random::uniform_real_distribution<double> dist(-1.0, 1.0);

  // Make data.
  std::array<double, MAPS_M * MAPS_N> Xm;
  std::array<double, MAPS_N> Yv;
  std::array<double, MAPS_M> Zv;
  fill(Xm, dist, gen);
  fill(Yv, dist, gen);

  // Do the math once to let the library initialize.
  cblas_dgemv(CblasRowMajor, CblasNoTrans, 60, 336, 1.0, Xm.data(), 336, Yv.data(), 1, 0.0, Zv.data(), 1);

  // Get ready record statistics about performance.
  boost::accumulators::accumulator_set<double,                                                          //
                                       boost::accumulators::stats<boost::accumulators::tag::mean,       //
                                                                  boost::accumulators::tag::max,        //
                                                                  boost::accumulators::tag::min,        //
                                                                  boost::accumulators::tag::variance>>  //
      loopStats = {};

  double u, v;
  for (int i = 0; i < CYCLES; i++) {
    // Set up for this iteration.
    fill(Xm, dist, gen);
    fill(Yv, dist, gen);

    u = timestamp();
    cblas_dgemv(CblasRowMajor, CblasNoTrans, 60, 336, 1.0, Xm.data(), 336, Yv.data(), 1, 0.0, Zv.data(), 1);
    v = timestamp();
    loopStats(v - u);
  }

  std::cout << boost::format("avg (min/max/var) : %g (%g, %g, %g) seconds") % boost::accumulators::mean(loopStats) %
                   boost::accumulators::min(loopStats) % boost::accumulators::max(loopStats) % boost::accumulators::variance(loopStats)
            << std::endl;
}