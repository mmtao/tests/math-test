#include <boost/array.hpp>
#include <boost/format.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/test/unit_test.hpp>
#include <iostream>

#pragma GCC diagnostic ignored "-Wdeprecated-copy"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>

// Number of cycles to measure over.
extern int CYCLES;

// A timestamp.
static inline double timestamp() {
  static double ten_to_the_nine = 1000000000.0;
  static struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  double retval = t.tv_nsec;
  retval /= ten_to_the_nine;
  retval += t.tv_sec;
  return retval;
}

static void fill(boost::numeric::ublas::vector<double> &v, boost::random::uniform_real_distribution<double> &dist, boost::mt19937 &gen) {
  for (uint32_t i = 0; i < v.size(); i++) {
    v(i) = dist(gen);
  }
}

static void fill(boost::numeric::ublas::matrix<double> &m, boost::random::uniform_real_distribution<double> &dist, boost::mt19937 &gen) {
  for (uint32_t i = 0; i < m.size1(); i++) {
    for (uint32_t j = 0; j < m.size2(); j++) {
      m(i, j) = dist(gen);
    }
  }
}

BOOST_AUTO_TEST_CASE(ublas) {
  std::cout << "-------------------" << std::endl << boost::format("Boost ublas %s, GCC %s, M/N/cycles: %d, %d, %d") % BOOST_LIB_VERSION % __VERSION__ % MAPS_M % MAPS_N % CYCLES << std::endl;
  std::cout << "Processor         : " << std::flush;
  system("lscpu | grep 'Model name' | sed -e 's/Model name:[ ]*//'");

  // Make random number generator.
  boost::mt19937 gen(1382958545);  // The 16th Bell number. https://oeis.org/A000110
  boost::random::uniform_real_distribution<double> dist(-1.0, 1.0);

  // Make input and output data.
  boost::numeric::ublas::vector<double> Yv(MAPS_N), Zv(MAPS_M);
  boost::numeric::ublas::matrix<double> Xm(MAPS_M, MAPS_N);
  fill(Xm, dist, gen);
  fill(Yv, dist, gen);

  // Do the math one to let the libraries initialize.
  Zv = boost::numeric::ublas::prod(Xm, Yv);

  // Get ready record statistics about performance.
  boost::accumulators::accumulator_set<double,                                                          //
                                       boost::accumulators::stats<boost::accumulators::tag::mean,       //
                                                                  boost::accumulators::tag::max,        //
                                                                  boost::accumulators::tag::min,        //
                                                                  boost::accumulators::tag::variance>>  //
      loopStats = {};

  double u, v;
  for (int i = 0; i < CYCLES; i++) {
    // Set up for this iteration.
    fill(Xm, dist, gen);
    fill(Yv, dist, gen);

    u = timestamp();
    Zv = boost::numeric::ublas::prod(Xm, Yv);
    v = timestamp();
    loopStats(v - u);
  }

  std::cout << boost::format("avg (min/max/var) : %g (%g, %g, %g) seconds") % boost::accumulators::mean(loopStats) %
                   boost::accumulators::min(loopStats) % boost::accumulators::max(loopStats) % boost::accumulators::variance(loopStats)
            << std::endl;
}